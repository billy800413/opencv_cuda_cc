# How to install OpenCV 4.5.2 with CUDA and CUDNN in Ubuntu, Ref [Git](https://gist.github.com/raulqf/f42c718a658cddc16f9df07ecc627be7)
### Requiremnet
|Package|Version|
|:--:|:--:|
|**ubuntu**|18.04|
|**GCC**|7.5.0 | 
|**Cmake**|min3.19.6|
|**OpenCV**|4.5.2|
|**cuda**|cuda_11.6.1_510.47.03_linux|
|**cudnn**|cudnn-linux-x86_64-8.3.2.44_cuda11.5|
|**Moeditor**|0.2.0|

## Ubuntu 18.04 System Setup
#### **1. basic**
```
$ sudo apt update
$ sudo apt upgrade
$ sudo apt install build-essential
```
#### **2. moeditor**  
##### [download link](https://moeditor.js.org/) 

## Install cmake 3.19.6
#### **1. Download**
##### [download link](https://cmake.org/files/)
or
```
wget https://github.com/Kitware/CMake/releases/download/v3.19.6/cmake-3.19.6.tar.gz
```
#### **2. Install**  
If the computer has a previous version installed
```
$ sudo apt autoremove cmake
```
Install cmake-3.19.6
```
$ sudo apt-get install libssl-dev
$ tar xzvf cmake-3.19.6.tar.gz
$ cd cmake-3.19.6
$ ./bootstrap
$ make -j 8
$ sudo make install
``` 
#### **3. set Environmental Variables**
```
$ sudo vim ~/.bashrc
>> export CMAKE_ROOT=/usr/local/share/cmake-3.19
$ source ~/.bashrc
```
#### **4. check cmake version**
```
$ cmake -version
cmake version 3.19.6
CMake suite maintained and supported by Kitware (kitware.com/cmake).
```

## Install cuda_11.6.1_510.47.03_linux
#### 1. Install Gpu driver(version 510 for example)
* Version >= 510
* Test this project withh version 510
* [CUDA Toolkit and Corresponding Driver Versions](https://docs.nvidia.com/cuda/cuda-toolkit-release-notes/index.html)  
```
$ sudo apt-get purge nvidia*
$ sudo add-apt-repository ppa:graphics-drivers
$ sudo apt-get update
$ sudo apt upgrade
$ sudo apt install nvidia-driver-510
$ sudo reboot
```
check driver
```
$ nvidia-smi
```

#### 2. Install cuda_11.6.1_510.47.03_linux
```
$ wget https://developer.download.nvidia.com/compute/cuda/11.6.1/local_installers/cuda_11.6.1_510.47.03_linux.run
$ sudo sh cuda_11.6.1_510.47.03_linux.run
```
If you have already installed the gpu driver, choose not to install the gpu driver provided by cuda.
#### 3. Add cuda Environment Variable
```
$ sudo vim ~/.bashrc
export PATH="/usr/local/cuda/bin:$PATH"
export LD_LIBRARY_PATH="/usr/local/cuda/lib64:$LD_LIBRARY_PATH"
$ source ~/.bashrc
```
check
```
$ nvcc -V
nvcc: NVIDIA (R) Cuda compiler driver
Copyright (c) 2005-2022 NVIDIA Corporation
Built on Thu_Feb_10_18:23:41_PST_2022
Cuda compilation tools, release 11.6, V11.6.112
Build cuda_11.6.r11.6/compiler.30978841_0
```

## Install cudnn Library cudnn-linux-x86_64-8.3.2.44_cuda11.5
Download [**cudnn cuDNN Library for Linux**](https://developer.nvidia.com/rdp/cudnn-download), you need to register an nvidia account.
```
$ tar Jxvf cudnn-linux-x86_64-8.3.2.44_cuda11.5-archive.tar.xz
$ sudo cp cuda/include/*.h /usr/local/cuda/include
$ sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64
$ sudo chmod a+r /usr/local/cuda/include/cudnn.h /usr/local/cuda/lib64/libcudnn*
```

## Opencv install
#### 1. Install required libraries:
* Generic tools:
```
$ sudo apt install build-essential cmake pkg-config unzip yasm git checkinstall
```
* Image I/O libs
``` 
$ sudo apt install libjpeg-dev libpng-dev libtiff-dev
``` 
* Video/Audio Libs - FFMPEG, GSTREAMER, x264 and so on.
```
$ sudo apt install libavcodec-dev libavformat-dev libswscale-dev libavresample-dev
$ sudo apt install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev
$ sudo apt install libxvidcore-dev x264 libx264-dev libfaac-dev libmp3lame-dev libtheora-dev 
$ sudo apt install libfaac-dev libmp3lame-dev libvorbis-dev
```
* OpenCore - Adaptive Multi Rate Narrow Band (AMRNB) and Wide Band (AMRWB) speech codec
```
$ sudo apt install libopencore-amrnb-dev libopencore-amrwb-dev
```   
* Cameras programming interface libs
```
$ sudo apt-get install libdc1394-22 libdc1394-22-dev libxine2-dev libv4l-dev v4l-utils
$ cd /usr/include/linux
$ sudo ln -s -f ../libv4l1-videodev.h videodev.h
$ cd ~
```
* GTK lib for the graphical user functionalites coming from OpenCV highghui module 
```
$ sudo apt-get install libgtk-3-dev
```
* Parallelism library C++ for CPU
```
$ sudo apt-get install libtbb-dev
```
* Optimization libraries for OpenCV
```
$ sudo apt-get install libatlas-base-dev gfortran
```
* Optional libraries:
```
$ sudo apt-get install libprotobuf-dev protobuf-compiler
$ sudo apt-get install libgoogle-glog-dev libgflags-dev
$ sudo apt-get install libgphoto2-dev libeigen3-dev libhdf5-dev doxygen
```

#### 2. We will now proceed with the installation (see the Qt flag that is disabled to do not have conflicts with Qt5.0).
```
$ cd ~/Downloads
$ wget -O opencv.zip https://github.com/opencv/opencv/archive/refs/tags/4.5.2.zip
$ wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/refs/tags/4.5.2.zip
$ unzip opencv.zip
$ unzip opencv_contrib.zip
```
Create a python conda environtment for the python binding module
```
$ conda create --name opencvBuild python=3.6
$ conda activate opencvBuild
$ pip install numpy
```
Procced with the installation
```
$ cd opencv-4.5.2
$ mkdir build
$ cd build
```
#### 3. Build from source 
Modify below flag CMAKE_INSTALL_PREFIX, CUDA_ARCH_BIN, OPENCV_PYTHON3_INSTALL_PATH, PYTHON_EXECUTABLE, OPENCV_EXTRA_MODULES_PATH
```
$ cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/opt/opencv-4.5.2 -D WITH_TBB=ON -D ENABLE_FAST_MATH=1 -D CUDA_FAST_MATH=1 -D WITH_CUBLAS=1 -D WITH_CUDA=ON -D BUILD_opencv_cudacodec=OFF -D WITH_CUDNN=ON -D OPENCV_DNN_CUDA=ON -D CUDA_ARCH_BIN=8.6 -D WITH_V4L=ON -D WITH_QT=OFF -D WITH_OPENGL=ON -D WITH_GSTREAMER=ON -D OPENCV_GENERATE_PKGCONFIG=ON -D OPENCV_PC_FILE_NAME=opencv.pc -D OPENCV_ENABLE_NONFREE=ON -D OPENCV_PYTHON3_INSTALL_PATH=/home/ubuntu/anaconda3/envs/opencvBuild/lib/python3.6/site-packages -D PYTHON_EXECUTABLE=/home/ubuntu/anaconda3/envs/opencvBuild/bin/python -D OPENCV_EXTRA_MODULES_PATH=/home/ubuntu/Downloads/opencv_contrib-4.5.2/modules -D INSTALL_PYTHON_EXAMPLES=OFF -D INSTALL_C_EXAMPLES=OFF -D BUILD_EXAMPLES=OFF ..
```
Note
* If you want to build the libraries statically you only have to include the *-D  BUILD_SHARED_LIBS=OFF*
* In case you do not want to include include CUDA set *-D WITH_CUDA=OFF*     
* If you want also to use CUDNN you must include those flags (to set the correct value of CUDA_ARCH_BIN you must visit https://developer.nvidia.com/cuda-gpus and find the Compute Capability CC of your graphic card).
```
-D WITH_CUDNN=ON
-D OPENCV_DNN_CUDA=ON
-D CUDA_ARCH_BIN=8.6
```

Before the compilation you must check that CUDA has been enabled in the configuration summary printed on the screen. 
```
--   NVIDIA CUDA:                   YES (ver 11.6, CUFFT CUBLAS FAST_MATH)
--     NVIDIA GPU arch:             86
--     NVIDIA PTX archs:
-- 
--   cuDNN:                         YES (ver 8.3.2)
```
If it is fine proceed with the compilation:
```
$ make -j8
$ sudo make install
```

#### 4. Include the libs in your environment(Not sure need to do this)
```
$ sudo /bin/bash -c 'echo "/usr/local/lib" >> /etc/ld.so.conf.d/opencv.conf'
$ sudo ldconfig
```