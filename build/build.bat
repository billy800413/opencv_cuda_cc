git clean -fdx
cmake ../ -G "Visual Studio 15 2017" -A x64
cmake --build . --config Release

ECHO %THIRD_PARTY%
xcopy   %THIRD_PARTY%\libtorch\libtorch-win-shared-with-deps-1.11.0+cu113\lib\*.dll  %~dp0\bin\Release
xcopy   %THIRD_PARTY%\libtorch\torchVision-v0.12.0-cu113\lib\*.dll  %~dp0\bin\Release
xcopy ..\lib\windows\*.dll %~dp0\bin\Release
xcopy  %THIRD_PARTY%\opencv\build\x64\vc15\bin\opencv_world349.dll  %~dp0\bin\Release

