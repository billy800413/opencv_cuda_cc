#include <iostream>
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudafilters.hpp>
#pragma once

class ImgTool
{
    public:
        ImgTool();
        ~ImgTool();
        void SetParameter();
        void Rotation(cv::Mat &src, cv::Mat &dst, float theta, cv::Point2f &rotationCenter);
        void Crop(cv::Mat &src, cv::Mat &dst, cv::Rect &roi);
        void KeepLargePixels(cv::Mat &src, cv::Mat &dst, int minPixels);
        void FindAngleWithHoughLinesP(cv::Mat &src);
        cv::RotatedRect FindMinAreaRect(cv::Mat &src);
        void RemoveColor(cv::Mat &src, cv::Mat &dst, int low_H, int low_S, int low_V, int high_H, int high_S, int high_V, cv::Mat &binaryMask);
        static bool ContourLengthSorter(std::vector<cv::Point> const&lhs, std::vector<cv::Point> const&rhs){
            return lhs.size() > rhs.size();
        }
        void CorrectionWithRemoveColorAndMinAreaRect(cv::Mat &src, cv::Mat &dst, int low_H, int low_S, int low_V, int high_H, int high_S, int high_V);
        double GetBinaryArea(cv::Mat &src);
        double GetBinaryArea(cv::cuda::GpuMat &src);
        cv::Mat m_src;
        cv::Mat m_dst;
};