#include <iostream>
#include <cmath>
#include <vector>
#include <basicFilterTool.h>
#include <json.hpp>

class SpatialFilterTool: public BasicFilterTool
{
    public:
        SpatialFilterTool();
        ~SpatialFilterTool();
        void SetParameter(nlohmann::json &filterConfig);
        void RunOperation(cv::Mat &src, cv::Mat &dst);
        void RunOperation(cv::cuda::GpuMat &src, cv::cuda::GpuMat &dst);

    private:
        cv::Mat m_kernal = cv::getGaussianKernel(3, 1.0);
        cv::Ptr<cv::cuda::Filter> m_kernalCudaPtr;
        cv::Size m_imgPaddingSize;
        cv::Size m_imgOriginalSize;
        std::string m_filterType;

};