#include <iostream>
#include <cmath>
#include <vector>
#include <basicFilterTool.h>
#include <json.hpp>

class FreqFilterTool: public BasicFilterTool
{
    public:
        FreqFilterTool();
        ~FreqFilterTool();
        void SetParameter(nlohmann::json &filterConfig);
        void RunOperation(cv::Mat &src, cv::Mat &dst);
        void RunOperation(cv::cuda::GpuMat &src, cv::cuda::GpuMat &dst);



    private:
        void CreateMask();

        void SaveMaskForDebug(cv::Mat &mask, std::string imgName);

        void ImageAddBorder(cv::Mat &src, cv::Mat &dst);
        void ImageAddBorder(cv::cuda::GpuMat &src, cv::cuda::GpuMat &dst);

        void FrequencyFilter(cv::Mat &src, cv::Mat &mask, cv::Mat &dst);
        void FrequencyFilter(cv::cuda::GpuMat &src, cv::cuda::GpuMat &mask, cv::cuda::GpuMat &dst);

        void Circshift(cv::Mat &out, const cv::Point &delta);
        void FFTshift(cv::Mat &out);
        void IFFTshift(cv::Mat &out);

        void HomoFilter(cv::Mat &src, cv::Mat &dst, cv::Mat &mask);
        void HomoFilter(cv::cuda::GpuMat &src, cv::cuda::GpuMat &dst, cv::cuda::GpuMat &mask);

        cv::Mat MakeGaussianHomoMask(cv::Size imgSize, float rh, float rl, float c, float D0);
        cv::Mat MakeBandStopFreqMask(cv::Size imgSize, int radius, int w, std::string type_="Ideal", int n=1);
        cv::Mat MakeLowPassFreqMask(cv::Size imgSize, int radius, std::string type_="Ideal", int n=1);


        bool m_normalFilter = true;
        cv::cuda::GpuMat m_maskCuda;
        cv::Mat m_mask;
        cv::Size m_imgPaddingSize;
        cv::Size m_imgOriginalSize;

};