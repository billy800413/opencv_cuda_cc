#include <string>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <json.hpp>

#include <filterChainGroup.h>

struct CvDetectionResult{
  cv::Mat anomalyMapsBinary;
  bool isOk;
};


class CvDetection{
    public:
        CvDetection();
        ~CvDetection();
        void SetCvDetection(std::string jsonPath);
        void RunCvDetection(cv::Mat &src);
        std::vector<CvDetectionResult> m_cvDetectionResults;
    private:
        std::string GetJsonString(std::string path);
        std::vector< std::unique_ptr<FilterChainGroup>  > m_DetectionVecPtr;

};