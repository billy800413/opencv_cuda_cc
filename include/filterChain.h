#include <string>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <json.hpp>


#include <basicFilterTool.h>
#include <freqFilterTool.h>
#include <spatialFilterTool.h>

class FilterChain
{
    public:
        FilterChain();
        ~FilterChain();
        void SetFilterChain(nlohmann::json &filterChainConfig);
        void RunFilterChain(cv::Mat &src);
        std::vector< std::unique_ptr<BasicFilterTool> > m_filterChainPtr;
        bool m_dstToCpu;
        cv::Mat m_dst;
        cv::cuda::GpuMat m_dstCuda;

};