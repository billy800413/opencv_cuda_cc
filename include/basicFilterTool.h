#include <iostream>
#include <string>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudafilters.hpp>

#include <json.hpp>

#ifndef _BasicFilterTool_
#define _BasicFilterTool_


class BasicFilterTool
{
    public:
        BasicFilterTool();
        ~BasicFilterTool();
        bool m_operationWithCpu;
        bool m_dstToCpu;
        cv::Mat m_src;
        cv::cuda::GpuMat m_srcCuda;
        cv::Mat m_dst;
        cv::cuda::GpuMat m_dstCuda;
        bool m_toFloatAndMultiplyRatio;
        float m_ratio;

        virtual void SetParameter(nlohmann::json &filterConfig);

        virtual void RunOperation(cv::Mat &src, cv::Mat &dst);
        virtual void RunOperation(cv::cuda::GpuMat &src, cv::cuda::GpuMat &dst);

        void ToFloatAndMultiplyRatio(cv::cuda::GpuMat &src, float ratio);
        void ToFloatAndMultiplyRatio(cv::Mat &src, float ratio);
        

        void SrcToOperationDevice(cv::Mat &src);
        void SrcToOperationDevice(cv::cuda::GpuMat &src);

        void DstToDevice();
        void Normalize(cv::Mat &src, cv::Mat &dst, cv::Size originalSize, double minval=0, double maxval=255);
        void Normalize(cv::cuda::GpuMat &src, cv::cuda::GpuMat &dst, cv::Size originalSize, double minval=0, double maxval=255);
        void ShowMatInfo(cv::Mat &src, std::string matName);
        void ShowMatInfo(cv::cuda::GpuMat &src, std::string matName);

        void Run(cv::Mat &src);
        void Run(cv::cuda::GpuMat &src);
        


};

#endif