#include <string>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <json.hpp>

#include <filterChain.h>

class FilterChainGroup{
    public:
        FilterChainGroup();
        ~FilterChainGroup();
        void SetFilterChainGroup(nlohmann::json &detectionConfig);
        void RunFilterChainGroup(cv::Mat &src);
        std::vector< std::unique_ptr<FilterChain>  > m_FilterChainGroupPtr;
        cv::Mat m_finalResultTh;
        bool m_isOk;

    private:
        cv::Mat m_finalResult;
        float m_thBoundrayLow;
        float m_thBoundrayHeigh;
        cv::Rect m_roi;
        int m_pixelsAllowed;
        bool m_binaryInversion;
        int GetBinaryPixels(cv::Mat &src);
};