#include "filterChainGroup.h"

FilterChainGroup::FilterChainGroup(){
    m_FilterChainGroupPtr.clear();
};

FilterChainGroup::~FilterChainGroup(){
    m_FilterChainGroupPtr.clear();
};

void FilterChainGroup::SetFilterChainGroup(nlohmann::json &detectionConfig){

    m_thBoundrayLow = detectionConfig["ThBoundrayLow"].get<float>();
    m_thBoundrayHeigh = detectionConfig["ThBoundrayHeigh"].get<float>();
    m_roi.x = detectionConfig["ROILeftTopWH"][0]; 
    m_roi.y = detectionConfig["ROILeftTopWH"][1]; 
    m_roi.width = detectionConfig["ROILeftTopWH"][2];
    m_roi.height = detectionConfig["ROILeftTopWH"][3];
    m_pixelsAllowed = detectionConfig["PixelsAllowed"].get<int>();
    m_binaryInversion = static_cast<bool>(detectionConfig["BinaryInversion"].get<int>());
    
    

    nlohmann::json filterChainGroupConfig = detectionConfig["FilterGroup"];

    m_FilterChainGroupPtr.resize(filterChainGroupConfig.size());
    for(int i=0; i<filterChainGroupConfig.size(); i++){
        nlohmann::json filterChainConfig = filterChainGroupConfig[i];
        m_FilterChainGroupPtr[i] = std::make_unique<FilterChain>();
        m_FilterChainGroupPtr[i]->SetFilterChain(filterChainConfig);
    }
}

int FilterChainGroup::GetBinaryPixels(cv::Mat &src){
    // src is a one channel binary uint8 binary(0, 255)
    int psum = cv::sum(src)[0] / 255;
    return psum;
}

void FilterChainGroup::RunFilterChainGroup(cv::Mat &src){
    cv::Mat srcWithRoi = src(m_roi);
    m_finalResult = cv::Mat(srcWithRoi.size(), CV_32FC1, cv::Scalar::all(1.0f));
    for(int i=0; i<m_FilterChainGroupPtr.size(); i++){
        m_FilterChainGroupPtr[i]->RunFilterChain(srcWithRoi);
        if (m_FilterChainGroupPtr[i]->m_dstToCpu){
            m_finalResult += m_FilterChainGroupPtr[i]->m_dst;
        }
        else{
            cv::Mat result;
            m_FilterChainGroupPtr[i]->m_dstCuda.download(result);
            m_finalResult += result;
        }
    }
    cv::inRange(m_finalResult, cv::Scalar(m_thBoundrayLow), cv::Scalar(m_thBoundrayHeigh), m_finalResultTh);

    if (m_binaryInversion){
        cv::bitwise_not(m_finalResultTh, m_finalResultTh);
    }

    if (GetBinaryPixels(m_finalResultTh) > m_pixelsAllowed){
        m_isOk = false;
    }
    else{
        m_isOk = true;
    }
}