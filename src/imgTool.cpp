#include "imgTool.h"

ImgTool::ImgTool(){
};

ImgTool::~ImgTool(){
};

void ImgTool::SetParameter(){

}

void ImgTool::Rotation(cv::Mat &src, cv::Mat &dst, float theta, cv::Point2f &rotationCenter){
  cv::Mat rotMat = cv::getRotationMatrix2D(rotationCenter, theta, 1.0);
  cv::warpAffine(src, dst, rotMat, src.size());
}

void ImgTool::Crop(cv::Mat &src, cv::Mat &dst, cv::Rect &roi){
    dst = src(roi);
}

void ImgTool::CorrectionWithRemoveColorAndMinAreaRect(cv::Mat &src, cv::Mat &dst, int low_H, int low_S, int low_V, int high_H, int high_S, int high_V){
  cv::Mat binaryMask;
  RemoveColor(src, dst, low_H, low_S, low_V, high_H, high_S, high_V, binaryMask);
  cv::RotatedRect rect = FindMinAreaRect(binaryMask);
  std::vector<cv::Point2f> boxPts(4);
  rect.points(boxPts.data());
  float angle = 0;
  if ( abs( boxPts[1].x - boxPts[3].x ) > 1e-5 ){
      float m = ( boxPts[3].y - boxPts[1].y ) / ( boxPts[1].x - boxPts[3].x );  // because y is minus
      angle = rect.angle;
      if (m > 0){
          angle = angle + 90;
      }
  }
  Rotation(src, dst, angle, rect.center);
}

void ImgTool::KeepLargePixels(cv::Mat &src, cv::Mat &dst, int minPixels){
  // src is a one channel binary uint8
  if (minPixels == 0){
    src.copyTo(dst);
    return;
  }
  dst = cv::Mat(src.size(), CV_8UC1, cv::Scalar::all(0));
  std::vector<std::vector<cv::Point>> contours;
  std::vector<cv::Vec4i> hierarchy;
  cv::findContours(src, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
  #pragma omp parallel for
  for(auto contour: contours){
    int area = (int)cv::contourArea(contour, false);
    int len = contour.size();
    int pixels = std::max(area, len);
    if (pixels > minPixels){
        cv::fillPoly(dst, std::vector<std::vector<cv::Point>>{contour}, cv::Scalar::all(255));
    }
  }
}

void ImgTool::FindAngleWithHoughLinesP(cv::Mat &src){
    // src is a one channel binary uint8
    cv::Mat debugImg = cv::Mat(src.size(), CV_8UC1, cv::Scalar::all(0));
    std::vector<cv::Vec4i> lines;
    cv::HoughLinesP(src, lines, 1, CV_PI / 180, 50, 30, 10);
    for (auto line: lines){
        cv::Point p0(line[0], line[1]);
        cv::Point p1(line[2], line[3]);
        if (p0.x > p1.x) {
            p0.x = line[2];
            p0.y = line[3];
            p1.x = line[0];
            p1.y = line[1];
        }
        cv::line(debugImg, cv::Point(line[0], line[1]), cv::Point(line[2], line[3]), cv::Scalar::all(255), 1, 8);
        double dx = p1.x - p0.x;
        double dy = p1.y - p0.y;
        double angle1 = std::atan(dy / dx);
        double angle2 = std::atan2(dy, dx);
        double d1 = (angle1 * 180.0) / M_PI;
        double d2 = (angle2 * 180.0) / M_PI;
        std::cout << "angle1: " << angle1 << " degree: " << d1 << std::endl;
        std::cout << "angle2: " << angle2 << " degree: " << d2 << std::endl;
    }
}


cv::RotatedRect ImgTool::FindMinAreaRect(cv::Mat &src){
    // src is a one channel binary uint8
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(src, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
    cv::RotatedRect rect;
    if (contours.size() == 0){
      return rect;
    }
    std::sort(contours.begin(), contours.end(), &ContourLengthSorter);
    rect = cv::minAreaRect( contours[0] );
    return rect;
}

void ImgTool::RemoveColor(cv::Mat &src, cv::Mat &dst, int low_H, int low_S, int low_V, int high_H, int high_S, int high_V, cv::Mat &binaryMask){
    cv::Mat srcHSV, inRange8UType;
    cv::cvtColor(src, srcHSV, cv::COLOR_BGR2HSV);
    cv::inRange(srcHSV, cv::Scalar(low_H, low_S, low_V), cv::Scalar(high_H, high_S, high_V), inRange8UType);
    binaryMask = cv::Mat(src.size(), CV_8UC1, cv::Scalar::all(255)) - inRange8UType;
    src.copyTo(dst, binaryMask);
}

double ImgTool::GetBinaryArea(cv::Mat &src){
  // src is a one channel binary uint8 binary(0, 255)
  // cv::Moments m = cv::moments(src, true);
  double psum = cv::sum(src)[0] / 255;
  return psum;
}

double ImgTool::GetBinaryArea(cv::cuda::GpuMat &src){
  // src is a one channel binary uint8 binary(0, 255)
  // cv::Moments m = cv::moments(src, true);
  double psum = cv::cuda::sum(src)[0] / 255;
  return psum;
}
