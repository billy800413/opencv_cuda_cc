#include "filterChain.h"

FilterChain::FilterChain(){
    m_filterChainPtr.clear();
};

FilterChain::~FilterChain(){
    m_filterChainPtr.clear();
};

void FilterChain::SetFilterChain(nlohmann::json &filterChainConfig){
    m_filterChainPtr.resize(filterChainConfig.size());
    nlohmann::json filterConfig;
    for(int i=0; i<filterChainConfig.size(); i++){
        filterConfig = filterChainConfig[i];
        if (filterConfig["type"] == "spatial"){
            m_filterChainPtr[i] = std::make_unique<SpatialFilterTool>();
        }
        else if(filterConfig["type"] == "freq"){
            m_filterChainPtr[i] = std::make_unique<FreqFilterTool>();
        }
        m_filterChainPtr[i]->SetParameter(filterConfig);
    }
    m_dstToCpu = static_cast<bool>(filterConfig["dstToCpu"].get<int>());
}

void FilterChain::RunFilterChain(cv::Mat &src){
    cv::Mat dst;
    cv::cuda::GpuMat dstCuda;
    
    for(int i=0; i<m_filterChainPtr.size(); i++){
        if(i == 0){
            m_filterChainPtr[i]->Run(src);
        }
        else{
            if (m_filterChainPtr[i -1]->m_dstToCpu){
                m_filterChainPtr[i]->Run(dst);
            }
            else{
                m_filterChainPtr[i]->Run(dstCuda);
            }
        }
        if (m_filterChainPtr[i]->m_dstToCpu){
            dst = m_filterChainPtr[i]->m_dst;
        }
        else{
            dstCuda = m_filterChainPtr[i]->m_dstCuda;
        }
    }

    if (m_dstToCpu){
        dst.copyTo(m_dst);
    }
    else{
        dstCuda.copyTo(m_dstCuda);
    }
}