#include "cvDetection.h"

CvDetection::CvDetection(){
    m_DetectionVecPtr.clear();
    m_cvDetectionResults.clear();
};

CvDetection::~CvDetection(){
    m_DetectionVecPtr.clear();
    m_cvDetectionResults.clear();
};


std::string CvDetection::GetJsonString(std::string path){
  std::shared_ptr<FILE> fp(fopen(path.c_str(), "r"),
                           [](FILE *file) { fclose(file); });
  if (fp == nullptr)
    return "";
  fseek(fp.get(), 0, SEEK_END);
  std::vector<char> content(ftell(fp.get()));
  fseek(fp.get(), 0, SEEK_SET);
  fread(content.data(), 1, content.size(), fp.get());
  std::string res = std::string(content.begin(), content.end());
  return res;
}

void CvDetection::SetCvDetection(std::string jsonPath){
    nlohmann::json jsonData = nlohmann::json::parse(GetJsonString(jsonPath));
    m_DetectionVecPtr.resize(jsonData["Detections"].size());
    m_cvDetectionResults.resize(jsonData["Detections"].size());

    for(int i=0; i<jsonData["Detections"].size(); i++){
        nlohmann::json detectionConfig = jsonData["Detections"][i];
        m_DetectionVecPtr[i] = std::make_unique<FilterChainGroup>();
        m_DetectionVecPtr[i]->SetFilterChainGroup(detectionConfig);
    }
}

void CvDetection::RunCvDetection(cv::Mat &src){
    for(int i=0; i<m_DetectionVecPtr.size(); i++){
        m_DetectionVecPtr[i]->RunFilterChainGroup(src);
        m_cvDetectionResults[i].isOk = m_DetectionVecPtr[i]->m_isOk;
        m_cvDetectionResults[i].anomalyMapsBinary = m_DetectionVecPtr[i]->m_finalResultTh;
    }
}
