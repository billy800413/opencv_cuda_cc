#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <cvDetection.h>
#include <json.hpp>



int main() {

  std::string jsonPath = "/home/ubuntu/Desktop/opencv_cuda_cc/jsonTest2.json";
  cv::String filename = "/home/ubuntu/Desktop/opencv_cuda_cc/Lenna.jpg";
  cv::Mat srcGray = cv::imread(filename, cv::IMREAD_GRAYSCALE);



  CvDetection cvDetection;
  cvDetection.SetCvDetection(jsonPath);
  cvDetection.RunCvDetection(srcGray);
  cv::imwrite("result.png", cvDetection.m_cvDetectionResults[0].anomalyMapsBinary);

}