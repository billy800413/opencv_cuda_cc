#include "spatialFilterTool.h"

SpatialFilterTool::SpatialFilterTool(){
};

SpatialFilterTool::~SpatialFilterTool(){
};


void SpatialFilterTool::SetParameter(nlohmann::json &filterConfig){
  m_operationWithCpu = static_cast<bool>(filterConfig["operationWithCpu"].get<int>());
  m_dstToCpu = static_cast<bool>(filterConfig["dstToCpu"].get<int>());
  m_filterType = filterConfig["filterType"];

  if(filterConfig.contains("ratio")){
      m_toFloatAndMultiplyRatio = true;
      m_ratio = filterConfig["ratio"].get<float>();
  }
  else{
      m_toFloatAndMultiplyRatio = false;
      m_ratio = 0;
  }

  if (m_operationWithCpu == false){
    m_kernalCudaPtr = cv::cuda::createLinearFilter(CV_32F, CV_32F, m_kernal);
  }
}


void SpatialFilterTool::RunOperation(cv::Mat &src, cv::Mat &dst){
  if (m_filterType == "BilateralFilter"){
    cv::bilateralFilter(src, dst, 5, 200, 200);
  }
  else{
    cv::filter2D(src, dst, CV_32F, m_kernal);
  }
}

void SpatialFilterTool::RunOperation(cv::cuda::GpuMat &src, cv::cuda::GpuMat &dst){
  // ref https://docs.opencv.org/4.x/d0/d05/group__cudaimgproc.html
  if (m_filterType == "BilateralFilter"){
    cv::cuda::bilateralFilter(src, dst, 5, 200, 200);
  }
  else{
    m_kernalCudaPtr->apply(src, dst);
  }
}