#include "basicFilterTool.h"

BasicFilterTool::BasicFilterTool(){
};

BasicFilterTool::~BasicFilterTool(){
};


void BasicFilterTool::SetParameter(nlohmann::json &filterConfig){
    std::cout<< "please run real SetParameter" << std::endl;
}

void BasicFilterTool::ToFloatAndMultiplyRatio(cv::cuda::GpuMat &src, float ratio){
    src.convertTo(src, CV_32F);
    cv::cuda::multiply(src, ratio, src);
}

void BasicFilterTool::ToFloatAndMultiplyRatio(cv::Mat &src, float ratio){
    src.convertTo(src, CV_32F);
    cv::multiply(src, ratio, src);
}


void BasicFilterTool::Normalize(cv::Mat &src, cv::Mat &dst, cv::Size originalSize, double minval, double maxval){
    cv::normalize(src, src, maxval, minval, cv::NORM_MINMAX, CV_8UC1);
    src.rowRange(0, originalSize.height).colRange(0, originalSize.width).copyTo(dst);
}
void BasicFilterTool::Normalize(cv::cuda::GpuMat &src, cv::cuda::GpuMat &dst, cv::Size originalSize, double minval, double maxval){
    cv::cuda::normalize(src, src, maxval, minval, cv::NORM_MINMAX, CV_8UC1);
    src.rowRange(0, originalSize.height).colRange(0, originalSize.width).copyTo(dst);
}

void BasicFilterTool::DstToDevice(){
    if (m_operationWithCpu && m_dstToCpu){ // cpu to cpu
        return;
    }
    else if(m_operationWithCpu == false && m_dstToCpu){ // gpu to cpu
        m_dstCuda.download(m_dst);
    }
    else if (m_operationWithCpu && m_dstToCpu == false){ // cpu to gpu
        m_dstCuda.upload(m_dst);
    }
    else{ // gpu to gpu
        return;
    }
}



void BasicFilterTool::ShowMatInfo(cv::Mat &src, std::string matName){
    std::cout<< matName + ".size(): " << src.size() << ", channel: " << src.channels() << ", type(): "<< src.type() << std::endl;
}

void BasicFilterTool::ShowMatInfo(cv::cuda::GpuMat &src, std::string matName){
    std::cout<< matName + ".size(): " << src.size() << ", channel: " << src.channels() << ", type(): "<< src.type() << std::endl;
}

void BasicFilterTool::SrcToOperationDevice(cv::Mat &src){
    src.copyTo(m_src);
    m_src.convertTo(m_src, CV_32F);
    if (m_operationWithCpu == false){
        m_srcCuda.upload(m_src);
    }
}

void BasicFilterTool::SrcToOperationDevice(cv::cuda::GpuMat &src){
  src.copyTo(m_srcCuda);
  m_srcCuda.convertTo(m_srcCuda, CV_32F);
  if (m_operationWithCpu){
    m_srcCuda.download(m_src);
  }
}

void BasicFilterTool::RunOperation(cv::Mat &src, cv::Mat &dst){
    std::cout<< "please run real operation" << std::endl;
}

void BasicFilterTool::RunOperation(cv::cuda::GpuMat &src, cv::cuda::GpuMat &dst){
    std::cout<< "please run real operation" << std::endl;
}


void BasicFilterTool::Run(cv::Mat &src){
    SrcToOperationDevice(src);
    if (m_operationWithCpu){
        RunOperation(m_src, m_dst);
    }
    else{
        RunOperation(m_srcCuda, m_dstCuda);
    }
    DstToDevice();
    
    if (m_toFloatAndMultiplyRatio){
        if (m_dstToCpu){
            ToFloatAndMultiplyRatio(m_dst, m_ratio);
        }
        else{
            ToFloatAndMultiplyRatio(m_dstCuda, m_ratio);
        }
    }
};

void BasicFilterTool::Run(cv::cuda::GpuMat &src){
    SrcToOperationDevice(src);
    if (m_operationWithCpu){
        RunOperation(m_src, m_dst);
    }
    else{
        RunOperation(m_srcCuda, m_dstCuda);
    }
    DstToDevice();

    if (m_toFloatAndMultiplyRatio){
        if (m_dstToCpu){
            ToFloatAndMultiplyRatio(m_dst, m_ratio);
        }
        else{
            ToFloatAndMultiplyRatio(m_dstCuda, m_ratio);
        }
    }
};