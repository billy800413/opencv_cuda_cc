#include "freqFilterTool.h"

FreqFilterTool::FreqFilterTool(){
};

FreqFilterTool::~FreqFilterTool(){
};


void FreqFilterTool::SetParameter(nlohmann::json &filterConfig){

  m_operationWithCpu = static_cast<bool>(filterConfig["operationWithCpu"].get<int>());
  m_dstToCpu = static_cast<bool>(filterConfig["dstToCpu"].get<int>());
  m_normalFilter = static_cast<bool>(filterConfig["normalFilter"].get<int>());
  m_imgOriginalSize.width = filterConfig["originalSizeWH"][0];
  m_imgOriginalSize.height = filterConfig["originalSizeWH"][1];

  if(filterConfig.contains("ratio")){
      m_toFloatAndMultiplyRatio = true;
      m_ratio = filterConfig["ratio"].get<float>();
  }
  else{
      m_toFloatAndMultiplyRatio = false;
      m_ratio = 0;
  }

  int height = cv::getOptimalDFTSize( m_imgOriginalSize.height );
  int width = cv::getOptimalDFTSize( m_imgOriginalSize.width );
  m_imgPaddingSize = cv::Size(width, height);
  // std::cout<< "m_imgOriginalSize, width: " << m_imgOriginalSize.width << ", height: " << m_imgOriginalSize.height << std::endl;
  // std::cout<< "m_imgPaddingSize, width: " << m_imgPaddingSize.width << ", height: " << m_imgPaddingSize.height << std::endl;
  CreateMask();
}


void FreqFilterTool::RunOperation(cv::Mat &src, cv::Mat &dst){
  cv::Mat srcPadding, ifft;
  ImageAddBorder(src, srcPadding);
  if (m_normalFilter){
        FrequencyFilter(srcPadding, m_mask, ifft);
  }
  else{
    HomoFilter(srcPadding, ifft, m_mask);
  }
  Normalize(ifft, m_dst, m_imgOriginalSize);
  if (m_toFloatAndMultiplyRatio){
      ToFloatAndMultiplyRatio(m_dst, m_ratio);
  }
}

void FreqFilterTool::RunOperation(cv::cuda::GpuMat &src, cv::cuda::GpuMat &dst){
  cv::cuda::GpuMat srcPaddingCuda, ifftCuda;
  ImageAddBorder(src, srcPaddingCuda);
  if (m_normalFilter){
        FrequencyFilter(srcPaddingCuda, m_maskCuda, ifftCuda);
  }
  else{
        HomoFilter(srcPaddingCuda, ifftCuda, m_maskCuda);
  }
  Normalize(ifftCuda, m_dstCuda, m_imgOriginalSize);
  if (m_toFloatAndMultiplyRatio){
      ToFloatAndMultiplyRatio(m_dstCuda, m_ratio);
  }
}

void FreqFilterTool::CreateMask(){
  cv::Mat onesMask = cv::Mat(m_imgPaddingSize, CV_32FC2, cv::Scalar::all(1.0f));
  int radius = 80;
  int w = 100;
  m_mask = MakeBandStopFreqMask(m_imgPaddingSize, radius, w, "Ideal");
  // m_mask = onesMask - m_mask;
  // m_mask = MakeGaussianHomoMask(m_imgPaddingSize, 2, 0.25, 1, 80 );
  SaveMaskForDebug(m_mask, "m_mask.png");
  // ShowMatInfo(m_mask, "m_mask");
  IFFTshift(m_mask);
  SaveMaskForDebug(m_mask, "m_maskShift.png");
  if (m_operationWithCpu == false){
    m_maskCuda.upload(m_mask);
    // ShowMatInfo(m_maskCuda, "m_maskCuda");
  }
}

void FreqFilterTool::SaveMaskForDebug(cv::Mat &mask, std::string imgName){
  std::vector<cv::Mat> channels(2);
  cv::split(mask, channels);
  cv::Mat maskDebug;
  channels[0].copyTo(maskDebug);
  cv::normalize(maskDebug, maskDebug, 255, 0, cv::NORM_MINMAX, CV_8UC1);
  // cv::imwrite(imgName, maskDebug);
}

void FreqFilterTool::ImageAddBorder(cv::Mat &src, cv::Mat &dst){
    int m = cv::getOptimalDFTSize( src.rows );
    int n = cv::getOptimalDFTSize( src.cols ); // on the border add zero values
    cv::copyMakeBorder(src, dst, 0, m - src.rows, 0, n - src.cols, cv::BORDER_CONSTANT, cv::Scalar::all(0));
}

void FreqFilterTool::ImageAddBorder(cv::cuda::GpuMat &src, cv::cuda::GpuMat &dst){
    int m = cv::getOptimalDFTSize( src.rows );
    int n = cv::getOptimalDFTSize( src.cols ); // on the border add zero values
    cv::cuda::copyMakeBorder(src, dst, 0, m - src.rows, 0, n - src.cols, cv::BORDER_CONSTANT, cv::Scalar::all(0));
}

void FreqFilterTool::FrequencyFilter(cv::Mat &src, cv::Mat &mask, cv::Mat &dst){
    src.convertTo(src, CV_32F);
    std::vector<cv::Mat> planes;
    planes.push_back(src);
    planes.push_back( cv::Mat(src.size(), CV_32F, 0.0f) );
    cv::Mat complexI;
    cv::merge(planes, complexI);         // Add to the expanded another plane with zeros
    cv::dft(complexI, complexI);            // this way the result may fit in the source matrix
    cv::multiply(complexI, mask, complexI);
    cv::idft(complexI, dst, cv::DFT_REAL_OUTPUT);
}

void FreqFilterTool::FrequencyFilter(cv::cuda::GpuMat &src, cv::cuda::GpuMat &mask, cv::cuda::GpuMat &dst){
    src.convertTo(src, CV_32F);
    std::vector<cv::cuda::GpuMat> planes;
    planes.push_back(src);
    planes.push_back( cv::cuda::GpuMat(src.size(), CV_32F, 0.0f) );
    cv::cuda::GpuMat complexI;
    cv::cuda::merge(planes, complexI);
    cv::cuda::dft(complexI, complexI, complexI.size());  // this way the result may fit in the source matrix
    cv::cuda::multiply(complexI, mask, complexI);
	cv::cuda::dft(complexI, complexI, complexI.size(), cv::DFT_INVERSE);
	cv::cuda::split(complexI, planes);
    planes[0].rowRange(0, src.rows).colRange(0, src.cols).copyTo(dst);
}

void FreqFilterTool::Circshift(cv::Mat &out, const cv::Point &delta){
    cv::Size sz = out.size();
    // 錯誤檢查
    assert(sz.height > 0 && sz.width > 0);
    // 此種情況不需要移動
    if ((sz.height == 1 && sz.width == 1) || (delta.x == 0 && delta.y == 0))
        return;
    // 需要移動參數的變換，這樣就能輸入各種整數了
    int x = delta.x;
    int y = delta.y;
    if (x > 0) x = x % sz.width;
    if (y > 0) y = y % sz.height;
    if (x < 0) x = x % sz.width + sz.width;
    if (y < 0) y = y % sz.height + sz.height;
    // 多維的Mat也能移動
    std::vector<cv::Mat> planes;
    cv::split(out, planes);
    for (size_t i = 0; i < planes.size(); i++)
    {
        // 豎直方向移動
        cv::Mat tmp0, tmp1, tmp2, tmp3;
        cv::Mat q0(planes[i], cv::Rect(0, 0, sz.width, sz.height - y));
        cv::Mat q1(planes[i], cv::Rect(0, sz.height - y, sz.width, y));
        q0.copyTo(tmp0);
        q1.copyTo(tmp1);
        tmp0.copyTo(planes[i](cv::Rect(0, y, sz.width, sz.height - y)));
        tmp1.copyTo(planes[i](cv::Rect(0, 0, sz.width, y)));

        // 水平方向移動
        cv::Mat q2(planes[i], cv::Rect(0, 0, sz.width - x, sz.height));
        cv::Mat q3(planes[i], cv::Rect(sz.width - x, 0, x, sz.height));
        q2.copyTo(tmp2);
        q3.copyTo(tmp3);
        tmp2.copyTo(planes[i](cv::Rect(x, 0, sz.width - x, sz.height)));
        tmp3.copyTo(planes[i](cv::Rect(0, 0, x, sz.height)));
    }
    cv::merge(planes, out);
}

void FreqFilterTool::FFTshift(cv::Mat &out){
    cv::Size sz = out.size();
    cv::Point pt(0, 0);
    pt.x = (int)floor(sz.width / 2.0);
    pt.y = (int)floor(sz.height / 2.0);
    Circshift(out, pt);
}

void FreqFilterTool::IFFTshift(cv::Mat &out){
    cv::Size sz = out.size();
    cv::Point pt(0, 0);
    pt.x = (int)ceil(sz.width / 2.0);
    pt.y = (int)ceil(sz.height / 2.0);
    Circshift(out, pt);
}

cv::Mat FreqFilterTool::MakeGaussianHomoMask(cv::Size imgSize, float rh, float rl, float c, float D0){
    cv::Mat gaussianHighPass(imgSize, CV_32FC2);
    int row_num = imgSize.height;
    int col_num = imgSize.width;
    float r = rh -rl;
    float d0 = 2 * D0 * D0;
    for(int i=0; i<row_num; i++ ){
        float *p = gaussianHighPass.ptr<float>(i);
        for(int j=0; j<col_num; j++ ){
            float d = pow((i - row_num/2),2) + pow((j - col_num/2),2);
            p[2*j]   = r*(1 - expf(-1*c*(d/d0))) + rl;
            p[2*j+1] = r*(1 - expf(-1*c*(d/d0))) + rl;
        }
    }
    return gaussianHighPass;
}

cv::Mat FreqFilterTool::MakeBandStopFreqMask(cv::Size imgSize, int radius, int w, std::string type_, int n){
    cv::Mat mask = cv::Mat(imgSize, CV_32FC2, cv::Scalar::all(1.0f));
    int midHeight  = int(imgSize.height / 2);
    int midWidth = int(imgSize.width / 2);
    for(int y= 0; y < imgSize.height; y++){
        for(int x= 0; x < imgSize.width; x++){
            float d = std::sqrt(std::pow(y - midHeight, 2) + std::pow(x - midWidth, 2)); // point(y,x) to the center(midHeight, midWidth) distance
            cv::Vec2f & color = mask.at<cv::Vec2f>(y,x);
            if ( type_ == "Ideal"){
                if ((radius - w / 2) < d && d < (radius + w / 2)){
                    color[0] = 0.0f;
                    color[1] = 0.0f;
                }
            }
            else if(type_ == "Butterworth"){
                float value = 1 / ( 1 +   std::pow( ( d * w /  (d *d - radius * radius) ), 2*n) );
                color[0] = value;
                color[1] = value;
            }
            else if(type_ == "Gaussian"){
                float value = 1 -  std::exp( -1 * std::pow( (d * d - radius * radius ) / (d * w), 2 )  );
                color[0] = value;
                color[1] = value;
            }
        }
    }
    return mask;
}

cv::Mat FreqFilterTool::MakeLowPassFreqMask(cv::Size imgSize, int radius, std::string type_, int n){
    cv::Mat mask = cv::Mat(imgSize, CV_32FC2, cv::Scalar::all(0.0f));
    int midHeight  = int(imgSize.height / 2);
    int midWidth = int(imgSize.width / 2);
    for(int y= 0; y < imgSize.height; y++){
        for(int x= 0; x < imgSize.width; x++){
            float d = std::sqrt(std::pow(y - midHeight, 2) + std::pow(x - midWidth, 2)); // point(y,x) to the center(midHeight, midWidth) distance
            cv::Vec2f & color = mask.at<cv::Vec2f>(y,x);
            if ( type_ == "Ideal"){
                if (d < radius){
                    color[0] = 1.0f;
                    color[1] = 1.0f;
                }
            }
            else if(type_ == "Butterworth"){
                float value = 1 / ( 1 +   std::pow( ( d / radius ), 2*n) );
                color[0] = value;
                color[1] = value;
            }
            else if(type_ == "Gaussian"){
                float value = std::exp( (-1 * d * d) / (2 * radius * radius)  );
                color[0] = value;
                color[1] = value;
            }
        }
    }
    return mask;
}

void FreqFilterTool::HomoFilter(cv::Mat &src, cv::Mat &dst, cv::Mat &mask){
    cv::log(src+1, src);
    cv::Mat ifft;
    FrequencyFilter(src, mask, ifft);
    cv::normalize(ifft, ifft, 5, 0, cv::NORM_MINMAX);
    cv::exp(ifft,ifft);
    dst = ifft - 1;
}

void FreqFilterTool::HomoFilter(cv::cuda::GpuMat &src, cv::cuda::GpuMat &dst, cv::cuda::GpuMat &mask){
    cv::cuda::add(src, 1, src);
    cv::cuda::log(src , src);
    cv::cuda::GpuMat ifft;
    FrequencyFilter(src, mask, ifft);
    cv::cuda::normalize(ifft, ifft, 5, 0, cv::NORM_MINMAX, CV_32FC1);
    cv::cuda::exp(ifft,ifft);
    cv::cuda::add(ifft, -1, dst);

}